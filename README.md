# Modular system front

For the project to work fully, you need to deploy the backend part: https://gitlab.com/university7234844

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run dev

```

## Create an https certificate

1. Для начала необходимо установить openssl: https://slproweb.com/products/Win32OpenSSL.html. 

2. После установки лучше перезапустить ПК. Если после перезагрузки в консоли команда openssl не определяется, то необходимо в переменную среды **Path** добавить путь к папке **bin**, установленного **Openssl**.

3. Далее в корне проекта создаём папку **https**

4. Переходим в созданную папку. В консоли генерируем приватный ключ: `openssl genrsa -out cert.key 2048`

5. Далее генерируем сертификат на основе полученного ключа: `openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 -nodes -keyout cert.key -out cert.crt -subj "/CN=localhost" -addext "subjectAltName=DNS:localhost,DNS:localhost,IP:127.0.0.1"`

6. Теперь, чтобы браузер доверял сертификату, необходимо открыть **настройки браузера** -> **конфиденциальность и безопасность** -> **безопасность**: chrome://settings/security

7. Пункт Настроить сертификаты (Управление настройками и сертификатами HTTPS/SSL).

8. В открывщемся меню необходимо вместо "Личные" выбрать "Доверенные центры сертификации" и нажать кнопку импорт.

9. В открывщемся окне, импортируем сертификат (cert.crt).

10. Перезагружаем браузер, проверяем, можем ли мы открыть сайт без предупреждений.

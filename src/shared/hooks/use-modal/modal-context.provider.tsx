import { createContext, PropsWithChildren, ReactNode, useState } from 'react'
import { createPortal } from 'react-dom'

export const ModalContext = createContext({} as ModalContext)

export interface ModalContext {
  showModal: (node: ReactNode) => void
  hideModal: VoidFunction
}

export const ModalContextProvider = ({ children }: PropsWithChildren) => {
  const [modalState, setModalState] = useState<ReactNode>(null)

  const showModal = (node: ReactNode) => {
    setModalState(node)
  }

  const hideModal = () => {
    setModalState(null)
  }

  const value = {
    showModal,
    hideModal,
  }

  return (
    <ModalContext.Provider value={value}>
      {createPortal(modalState, document.body)}
      {children}
    </ModalContext.Provider>
  )
}

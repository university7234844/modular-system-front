import { FunctionComponent, useContext } from 'react'

import { BaseModalProps } from '@/shared/components'

import { ModalContext } from './modal-context.provider'

export const useModal = <T extends BaseModalProps = BaseModalProps>(
  Modal: FunctionComponent<T>,
  props?: Omit<T, 'onClose'>
) => {
  const { hideModal, showModal } = useContext(ModalContext)

  const handleShow = () => {
    showModal(<Modal {...(props as T)} onClose={hideModal} />)
  }

  return {
    showModal: handleShow,
    hideModal,
  }
}

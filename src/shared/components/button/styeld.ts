import styled, { css } from 'styled-components'

export const StyledButton = styled.button`
  background-color: transparent;
  ${({ theme }) => css`
    border: 1px solid ${theme.colors.while};
    border-radius: ${theme.borderRadiuses.s};
  `}
`

import { ButtonHTMLAttributes, forwardRef, PropsWithChildren } from 'react'

import { StyledButton } from './styeld'

interface ButtonProps
  extends Omit<ButtonHTMLAttributes<HTMLButtonElement>, 'children'>,
    PropsWithChildren {}

export const Button = forwardRef<HTMLButtonElement, ButtonProps>(
  ({ children, ...props }, ref) => {
    return (
      <StyledButton {...props} ref={ref}>
        {children}
      </StyledButton>
    )
  }
)

import { LoginModal } from '@/shared/components/auth-modal/login-modal/login-modal'
import { useModal } from '@/shared/hooks/use-modal/use-modal'

import { LoginButton, LoginContainer } from './styles'

export const Login = () => {
  const { showModal } = useModal(LoginModal)

  return (
    <LoginContainer>
      <LoginButton onClick={showModal}>Login</LoginButton>
    </LoginContainer>
  )
}

import styled, { css } from 'styled-components'

export const LoginContainer = styled.div`
  display: flex;
`

export const LoginButton = styled.button`
  ${({ theme }) => css`
    padding: ${theme.spaces.xs2}px 1em;
    color: ${theme.colors.lightSkyBlue};
    font-weight: 100;
    background-color: transparent;
    transition: all 350ms ease-in-out;
    cursor: pointer;

    &:hover {
      color: ${theme.colors.brightOrange};
      background-color: transparent;
    }
  `}
`

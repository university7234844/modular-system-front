import styled, { css } from 'styled-components'

export const NavbarContainer = styled.div`
  ${({ theme }) => css`
    ${theme.getTypography('desktop', 'title', 'title3')}

    box-shadow: 0 1px ${theme.spaces.xs3}px ${theme.colors.lightSkyBlue};
    padding: 0 ${theme.spaces.sm}px;

    ${theme.below.laptop`
       padding: 0 ${theme.spaces.xs2}px;
    `}
  `}

  height: 60px;
  display: flex;
  align-items: center;
`

export const LeftSection = styled.div`
  display: flex;
`

export const MiddleSection = styled.div`
  display: flex;
  flex: 2;
  height: 100%;
  justify-content: space-evenly;
`

export const RightSection = styled.div`
  display: flex;
`

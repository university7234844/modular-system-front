import { useRef, useState } from 'react'

import { useOnClickOutside } from '@/shared/hooks'

import { Hamburger } from '../burger-menu'
import { StyledLink, StyledMenu } from './styles'

export const Menu = () => {
  const [open, setOpen] = useState<boolean>(false)
  const wrapperRef = useRef<HTMLDivElement>(null)
  const handleClose = () => setOpen(false)

  useOnClickOutside(wrapperRef, handleClose)

  return (
    <div ref={wrapperRef}>
      <StyledMenu open={open}>
        <StyledLink onClick={handleClose}>Link 1</StyledLink>
        <StyledLink onClick={handleClose}>Link 2</StyledLink>
        <StyledLink onClick={handleClose}>Link 3</StyledLink>
      </StyledMenu>
      <Hamburger open={open} setOpen={setOpen} />
    </div>
  )
}

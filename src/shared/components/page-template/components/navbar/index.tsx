import { useMediaQuery } from 'react-responsive'

import { useSingedUser } from '@/state/user/context/use-singed-user'
import { SCREEN_SIZES } from '@/theme'

import { Logo } from '../logo'
import { links } from './config'
import { Login } from './login'
import { Logout } from './logout'
import { Menu } from './menu'
import { NavLinks } from './nav-links'
import {
  LeftSection,
  MiddleSection,
  NavbarContainer,
  RightSection,
} from './styles'

export function Navbar() {
  const isMobile = useMediaQuery({ maxWidth: SCREEN_SIZES.laptop })
  const { user } = useSingedUser()
  return (
    <NavbarContainer>
      <LeftSection>
        <Logo />
      </LeftSection>
      <MiddleSection>{!isMobile && <NavLinks links={links} />}</MiddleSection>
      <RightSection>
        {isMobile && <Menu />}
        {!isMobile && (user ? <Logout /> : <Login />)}
      </RightSection>
    </NavbarContainer>
  )
}

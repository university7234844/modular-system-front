import { useNavigate } from 'react-router-dom'

import { Link, LinkItem, LinkWrapper, NavLinksContainer } from './styles'
import { NavLinksProps } from './types'

export const NavLinks: React.FC<NavLinksProps> = ({ links }) => {
  const navigate = useNavigate()

  const handleNavigate = (link: string) => () => {
    navigate(link)
  }

  return (
    <NavLinksContainer>
      <LinkWrapper>
        {links.map(({ href, label }, index) => (
          <LinkItem key={index}>
            <Link onClick={handleNavigate(href)}>{label}</Link>
          </LinkItem>
        ))}
      </LinkWrapper>
    </NavLinksContainer>
  )
}

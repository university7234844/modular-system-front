import styled, { css } from 'styled-components'

export const NavLinksContainer = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
`

export const LinkWrapper = styled.ul`
  margin: 0;
  padding: 0;
  display: flex;
  height: 100%;
  list-style: none;
`

export const LinkItem = styled.div`
  ${({ theme }) => css`
    height: 100%;
    padding: 0 ${theme.spaces.sm}px;
    color: ${theme.colors.lightSkyBlue};
    align-items: center;
    justify-content: center;
    display: flex;
    border-top: 2px solid transparent;
    transition: all 350ms ease-in-out;

    &:hover {
      border-top: 2px solid ${theme.colors.brightOrange};
    }

    ${theme.below.laptop`
      padding: 0 ${theme.spaces.xs2}px;
    `}
  `}
`

export const Link = styled.a`
  ${({ theme }) => css`
    ${theme.getTypography('desktop', 'title', 'title3')}
    font-weight: 100;
    text-decoration: none;
    color: inherit;
    transition: all 350ms ease-in-out;

    &:hover {
      color: ${theme.colors.brightOrange};
      cursor: pointer;
    }

    ${theme.below.laptop`
      ${theme.getTypography('desktop', 'title', 'title4')}
    `}
  `}
`

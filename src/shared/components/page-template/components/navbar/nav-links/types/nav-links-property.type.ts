import { NavLink } from './nav-link.type'

export type NavLinksProps = {
  links: NavLink[]
}

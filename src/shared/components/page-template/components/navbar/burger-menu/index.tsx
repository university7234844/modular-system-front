import { StyledHamburger } from './styles'
import { MenuProps } from './types'

export const Hamburger = ({ open, setOpen }: MenuProps) => {
  const handleClick = () => {
    setOpen(!open)
  }

  return (
    <StyledHamburger open={open} onClick={handleClick}>
      <div />
      <div />
      <div />
    </StyledHamburger>
  )
}

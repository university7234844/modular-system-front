export type MenuProps = {
  open: boolean
  setOpen: (v: boolean) => void
}

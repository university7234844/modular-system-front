import { NavLink } from './nav-links/types'

export const links: NavLink[] = [
  { label: 'Главная', href: '/' },
  { label: 'Специальности', href: '/specialties' },
  { label: 'Кафедры', href: '#explore' },
  { label: 'Студенты', href: '#discover' },
  { label: 'Деканат', href: '#contact' },
]

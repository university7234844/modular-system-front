import { useLogoutUserMutation } from '@/state/auth/mutations/use-logout-mutation'
import { useSingedUser } from '@/state/user/context/use-singed-user'

export const Logout = () => {
  const { setUser } = useSingedUser()

  const onSuccess = () => {
    setUser(null)
  }

  const { mutateAsync } = useLogoutUserMutation({ onSuccess })

  const handleClick = () => {
    mutateAsync()
  }

  return <div onClick={handleClick}>Logout</div>
}

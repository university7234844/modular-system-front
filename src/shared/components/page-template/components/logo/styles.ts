import styled, { css } from 'styled-components'

export const LogoWrapper = styled.div`
  display: flex;
  align-items: center;
`

export const LogoImg = styled.div`
  ${({ theme }) => css`
    width: 45px;
    height: 45px;

    img {
      height: 100%;
    }

    ${theme.below.laptop`
      width: 35px;
      height: 35px;
    `}
  `}
`

export const LogoText = styled.h2`
  ${({ theme }) => css`
    ${theme.getTypography('desktop', 'title', 'large')}

    margin: 0;
    margin-left: ${theme.spaces.xs}px;
    color: ${theme.colors.lightSkyBlue};
  `}
`

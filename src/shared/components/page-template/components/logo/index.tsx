import logo from '@/assets/vsu-logo.jpg'

import { LogoImg, LogoText, LogoWrapper } from './styles'

export function Logo() {
  return (
    <LogoWrapper>
      <LogoImg>
        <img src={logo} alt="logoimg" />
      </LogoImg>
      <LogoText>Modular system</LogoText>
    </LogoWrapper>
  )
}

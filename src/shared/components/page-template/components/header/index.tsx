import { Navbar } from '../navbar'
import { StyledHeader } from './styles'

export const Header = () => (
  <StyledHeader>
    <Navbar />
  </StyledHeader>
)

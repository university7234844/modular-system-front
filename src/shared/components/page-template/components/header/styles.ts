import styled, { css } from 'styled-components'

export const StyledHeader = styled.header`
  ${({ theme }) => css`
    background-color: ${theme.colors.darkGray};
    color: white;
    padding: ${theme.spaces.xs2}px ${theme.spaces.xs}px;
    z-index: ${theme.zIndexes['index-1']};
  `}

  text-align: center;
  width: 100%;
  top: 0;
  left: 0;
  box-sizing: border-box;
`

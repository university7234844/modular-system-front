import { Link } from '@/types/footer-link.type'

export type FooterProps = {
  links: Link[]
  phoneNumber: string
}

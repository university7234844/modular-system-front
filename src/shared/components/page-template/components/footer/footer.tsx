import {
  AboutUsContainer,
  FooterContainer,
  FooterLink,
  PhoneNumber,
} from './styles'
import { FooterProps } from './types'

const date = new Date().getFullYear()

export const Footer: React.FC<FooterProps> = ({ links, phoneNumber }) => {
  return (
    <FooterContainer>
      <AboutUsContainer>
        <PhoneNumber>Phone: {phoneNumber}</PhoneNumber>
        <div>© {date} Modular system.</div>
      </AboutUsContainer>
      {links.map(({ title, url }, index) => (
        <FooterLink
          key={index}
          href={url}
          target="_blank"
          rel="noopener noreferrer"
        >
          {title}
        </FooterLink>
      ))}
    </FooterContainer>
  )
}

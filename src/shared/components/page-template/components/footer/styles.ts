import styled, { css } from 'styled-components'

export const FooterContainer = styled.footer`
  ${({ theme }) => css`
    ${theme.getTypography('desktop', 'title', 'title3')}

    background-color: ${theme.colors.darkGray};
    color: ${theme.colors.lightSkyBlue};
    padding: ${theme.spaces.xs2}px ${theme.spaces.xs}px;
  `}

  text-align: center;
  width: 100%;
  bottom: 0;
  left: 0;
  box-sizing: border-box;
  display: flex;
`

export const AboutUsContainer = styled.div``

export const FooterLink = styled.a`
  ${({ theme }) => css`
    margin: 0 ${theme.spaces.xs}px;
    color: ${theme.colors.lightSkyBlue};
    text-decoration: none;

    &:hover {
      color: ${theme.colors.brightOrange};
    }
  `}
`

export const PhoneNumber = styled.div`
  ${({ theme }) => css`
    margin-top: ${theme.spaces.xs3}px;
  `}
`

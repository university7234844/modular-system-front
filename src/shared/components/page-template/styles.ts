import styled, { css } from 'styled-components'

export const MainContainer = styled.div`
  ${({ theme }) => css`
    ${theme.getTypography('desktop', 'title', 'title5')}

    background-color: ${theme.colors.darkGray};
  `}

  display: flex;
  justify-content: space-between;
  flex-direction: column;
  align-items: center;
  box-sizing: border-box;
  min-height: 100vh;
`
export const ContentDiv = styled.div`
  ${({ theme }) => css`
    padding: ${theme.spaces.sm}px;
    border: 1px solid ${theme.colors.white}; // Пример границы
  `}

  width: 100%;
  text-align: center;
  box-sizing: border-box;
  min-height: 100vh;
`

import { Outlet } from 'react-router-dom'

import { footerLinks } from '@/constants'
import { getBuildEnv } from '@/utils/get-build-env'

import { Footer, Header } from './components'
import { ContentDiv, MainContainer } from './styles'

export const PageTemplate = () => {
  return (
    <MainContainer>
      <Header />
      <ContentDiv>
        <Outlet />
      </ContentDiv>
      <Footer links={footerLinks} phoneNumber={getBuildEnv('PHONE_NUMBER')} />
    </MainContainer>
  )
}

import { forwardRef, InputHTMLAttributes } from 'react'

import { InputWrapper, StyledInput, StyledLabel } from './styled'

export interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  label: string
}

export const Input = forwardRef<HTMLInputElement, InputProps>(
  ({ label, ...props }: InputProps, ref) => {
    return (
      <div>
        {label && <StyledLabel>{label}</StyledLabel>}
        <InputWrapper>
          <StyledInput {...props} ref={ref} />
        </InputWrapper>
      </div>
    )
  }
)

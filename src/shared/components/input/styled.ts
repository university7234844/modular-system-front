import styled, { css } from 'styled-components'

export const InputWrapper = styled.div`
  ${({ theme }) => css`
    border: 1px solid ${theme.colors.while};
    padding: ${theme.spaces.xs2}px ${theme.spaces.xs}px;
  `}
`

export const StyledInput = styled.input`
  all: unset;
  width: 100%;
`

export const StyledLabel = styled.h3`
  ${({ theme }) => css`
    margin: 0 0 ${theme.spaces.xs2}px 0;
    ${theme.getTypography('desktop', 'title', 'title3')};
    font-weight: 400;
  `}
`

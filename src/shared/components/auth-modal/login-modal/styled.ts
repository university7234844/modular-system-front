import { css, styled } from 'styled-components'

export const ModalContainer = styled.div`
  ${({ theme }) => css`
    min-width: ${theme.modalWidths.m};
    background-color: ${theme.colors.darkGray};
    border-radius: ${theme.borderRadiuses.m};
    padding: ${theme.spaces.xs}px;
  `}
`

export const Title = styled.h1`
  ${({ theme }) => css`
    ${theme.getTypography('desktop', 'title', 'large')}
  `}
`

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  ${({ theme }) => css`
    gap: ${theme.spaces.xs2}px;
  `}
`

export const ButtonRow = styled.div`
  display: flex;
  ${({ theme }) => css`
    gap: ${theme.spaces.xs2}px;
  `}
`

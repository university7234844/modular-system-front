import { useForm } from 'react-hook-form'

import { LoginUserRequest } from '@/api/api-client/auth/types/login-user.request'
import { useLoginUserMutation } from '@/state/auth/mutations/use-login-mutation'
import { useSingedUser } from '@/state/user/context/use-singed-user'
import { User } from '@/state/user/context/user-context-provider'

import { Button } from '../../button'
import { Input } from '../../input/input'
import { BaseModalProps, Modal } from '../../modal/modal'
import { ButtonRow, Form, ModalContainer, Title } from './styled'

interface LoginModalProps extends BaseModalProps {}

export const LoginModal = ({ onClose }: LoginModalProps) => {
  const { setUser } = useSingedUser()

  const { handleSubmit, register } = useForm({
    defaultValues: {
      login: '',
      password: '',
    },
  })

  const onUserLoggedIn = (user: User) => {
    setUser(user)
    onClose()
  }

  const onSubmit = (data: LoginUserRequest) => {
    handleLogin(data)
  }

  const { mutateAsync: handleLogin } = useLoginUserMutation({
    onSuccess: onUserLoggedIn,
  })

  return (
    <Modal>
      <ModalContainer>
        <Title>Вход</Title>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Input {...register('login')} label="Логин:" />
          <Input {...register('password')} label="Пароль:" type="password" />
          <ButtonRow>
            <Button type="submit">Войти</Button>
            <Button onClick={onClose}>Закрыть</Button>
          </ButtonRow>
        </Form>
      </ModalContainer>
    </Modal>
  )
}

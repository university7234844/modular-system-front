import { PropsWithChildren, useEffect } from 'react'

import { ModalWrapper } from './styeld'

export interface BaseModalProps {
  onClose: VoidFunction
}

export const Modal = ({ children }: PropsWithChildren) => {
  useEffect(() => {
    document.body.style.setProperty('overflow-y', 'hidden')

    return () => {
      document.body.style.setProperty('overflow-y', 'unset')
    }
  }, [])

  return <ModalWrapper>{children}</ModalWrapper>
}

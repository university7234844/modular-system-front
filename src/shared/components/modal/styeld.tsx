import styled, { css } from 'styled-components'

export const ModalWrapper = styled.div`
  ${({ theme }) => css`
    position: sticky;
    z-index: ${theme.zIndexes['index-3']};
    background-color: ${theme.colors.darkTransparent};
  `}
  bottom: 0;
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`

import {
  createContext,
  Dispatch,
  PropsWithChildren,
  SetStateAction,
  useLayoutEffect,
  useState,
} from 'react'

import { LocalStorageClient } from '@/api/local-storage-client'

import { User } from '../../types'
import { useCurrentUserQuery } from '../queries/use-current-user-query'

interface UserContextType {
  user?: User | null
  setUser: Dispatch<SetStateAction<User | null>>
}

export const UserContext = createContext({} as UserContextType)

export const UserContextProvider = ({ children }: PropsWithChildren) => {
  const [user, setUser] = useState<User | null>(null)
  const { data } = useCurrentUserQuery()

  useLayoutEffect(() => {
    if (data && LocalStorageClient.getAccessToken()) setUser(data)
  }, [data])

  return (
    <UserContext.Provider value={{ setUser, user }}>
      {children}
    </UserContext.Provider>
  )
}

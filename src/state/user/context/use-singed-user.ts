import { useContext } from 'react'

import { UserContext } from './user-context-provider'

export const useSingedUser = () => useContext(UserContext)

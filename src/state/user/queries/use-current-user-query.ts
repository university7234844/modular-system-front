import { useQuery } from 'react-query'

import { useApi } from '@/api'

import { userKeys } from '../keys'


export const useCurrentUserQuery = () => {
  const { userClient } = useApi()

  const queryFn = async () => {
    const { data } = await userClient.getCurrentUser()
    return data
  }

  return useQuery({ queryFn, queryKey: userKeys.all, refetchOnMount: false, retry: false })
}

import { useMutation } from 'react-query'

import { LoginUserResponse } from '@/api/api-client/auth/types/login-user.response'
import { useApi } from '@/api/api-client/base/use-api'

interface UseRefreshJWTMutation {
  onError?: VoidFunction
  onSuccess?: (data: LoginUserResponse) => void
}

export const useRefreshJWTMutation = ({
  onError,
  onSuccess,
}: UseRefreshJWTMutation) => {
  const { authClient } = useApi()

  const mutationFn = async () => {
    const { data } = await authClient.refreshLogin()
    return data
  }

  return useMutation({
    mutationFn,
    onError,
    onSuccess,
  })
}

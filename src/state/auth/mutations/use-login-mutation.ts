import { useMutation } from 'react-query'

import { LoginUserRequest } from '@/api/api-client/auth/types/login-user.request'
import { LoginUserResponse } from '@/api/api-client/auth/types/login-user.response'
import { useApi } from '@/api/api-client/base/use-api'

export interface UseLoginUserMutationProps {
  onError?: VoidFunction
  onSuccess?: (data: LoginUserResponse) => void
}

export const useLoginUserMutation = ({
  onError,
  onSuccess,
}: UseLoginUserMutationProps) => {
  const { authClient } = useApi()

  const mutationFn = async (request: LoginUserRequest) => {
    const { data } = await authClient.loginUser(request)
    return data
  }

  return useMutation({
    mutationFn,
    onError,
    onSuccess,
  })
}

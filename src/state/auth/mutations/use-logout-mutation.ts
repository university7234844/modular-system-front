import { useMutation } from 'react-query'

import { useApi } from '@/api/api-client/base/use-api'

export interface useLogoutUserMutationProps {
  onError?: () => void
  onSuccess?: () => void
}

export const useLogoutUserMutation = ({
  onError,
  onSuccess,
}: useLogoutUserMutationProps) => {
  const { authClient } = useApi()

  const mutationFn = () => {
    return authClient.logoutUser()
  }

  return useMutation({
    mutationFn,
    onError,
    onSuccess,
  })
}

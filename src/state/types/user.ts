import { Role } from "@/api"

export interface User {
  id: number
  login: string
  roles: Role[]
}

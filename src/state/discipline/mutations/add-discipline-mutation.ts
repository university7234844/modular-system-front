import { useMutation, useQueryClient } from 'react-query'

import { useApi } from '@/api/api-client/base/use-api'
import { Discipline } from '@/api/api-client/discipline/types'

import { disciplineKeys } from '../keys'

export interface useAddDisciplineMutationProps {
  onError?: VoidFunction
  onSuccess?: VoidFunction
}

export const useAddDisciplineMutation = ({
  onError,
  onSuccess,
}: useAddDisciplineMutationProps = {}) => {
  const { disciplineClient } = useApi()

  const queryClient = useQueryClient()
  const mutationFn = async (discipline: Omit<Discipline, 'id'>) => {
    const { data } = await disciplineClient.addDiscipline(discipline)
    return data
  }

  const onMutationSuccess = async () => {
    await queryClient.invalidateQueries(disciplineKeys.all)

    onSuccess?.()
  }

  return useMutation({
    mutationFn,
    onError,
    onSuccess: onMutationSuccess,
  })
}

import { useMutation, useQueryClient } from 'react-query'

import { useApi } from '@/api/api-client/base/use-api'

import { disciplineKeys } from '../keys'

export interface UseDeleteDisciplineMutationProps {
  onError?: VoidFunction
  onSuccess?: VoidFunction
}

export const useDeleteDisciplineMutation = ({
  onError,
  onSuccess,
}: UseDeleteDisciplineMutationProps = {}) => {
  const { disciplineClient } = useApi()

  const queryClient = useQueryClient()
  const mutationFn = async (id: number) => {
    const { data } = await disciplineClient.deleteDiscipline(id)
    return data
  }

  const onMutationSuccess = async () => {
    await queryClient.invalidateQueries({ queryKey: disciplineKeys.all })
    onSuccess?.()
  }

  return useMutation({
    mutationFn,
    onError,
    onSuccess: onMutationSuccess,
  })
}

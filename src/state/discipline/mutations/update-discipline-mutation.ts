import { useMutation, useQueryClient } from 'react-query'

import { useApi } from '@/api/api-client/base/use-api'
import { Discipline } from '@/api/api-client/discipline/types'

import { disciplineKeys } from '../keys'

export interface useUpdateDisciplineMutationProps {
  onError?: VoidFunction
  onSuccess?: VoidFunction
}

interface MutationParams {
  id: number
  discipline: Omit<Discipline, 'id'>
}

export const useUpdateDisciplineMutation = ({
  onError,
  onSuccess,
}: useUpdateDisciplineMutationProps = {}) => {
  const { disciplineClient } = useApi()

  const queryClient = useQueryClient()

  const mutationFn = async ({ discipline, id }: MutationParams) => {
    const { data } = await disciplineClient.updateDiscipline(id, discipline)
    return data
  }

  const onMutationSuccess = async () => {
    await queryClient.invalidateQueries({ queryKey: disciplineKeys.all })

    onSuccess?.()
  }

  return useMutation({
    mutationFn,
    onError,
    onSuccess: onMutationSuccess,
  })
}

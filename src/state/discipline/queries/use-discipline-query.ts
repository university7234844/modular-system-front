import { useQuery } from 'react-query'

import { useApi } from '@/api'

import { disciplineKeys } from '../keys'

export const useDisciplineQuery = () => {
  const { disciplineClient } = useApi()

  const queryFn = async () => {
    const { data } = await disciplineClient.getDiscipline()
    return data
  }

  return useQuery({ queryFn, queryKey: disciplineKeys.all })
}

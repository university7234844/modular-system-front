type Title =
  | 'large'
  | 'blog'
  | 'title1'
  | 'title2'
  | 'title3'
  | 'title4'
  | 'title5'
type MobileBody = 'body1' | 'body2' | 'body3' | 'body4'
type Body = 'body1' | 'body2' | 'body3'
type Button = 'normal' | 'medium' | 'small'
type PreText = 'text1'
export type Device = 'desktop' | 'mobile'
export type ComponentVariant = 'title' | 'body' | 'button' | 'preText'

export type Size = {
  fontSize: number
  fontWeight: number
  lineHeight: number
}

export type ComponentVariants = {
  desktop: {
    title: Title
    button: Button
    body: Body
    preText: PreText
  }
  mobile: {
    title: Title
    body: MobileBody
    button: Exclude<Button, 'small'>
    preText: PreText
  }
}

export type Typography = {
  [device in Device]: {
    [variant in ComponentVariant]: {
      [key in ComponentVariants[device][variant]]: Size
    }
  }
}

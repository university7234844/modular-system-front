import { Styles } from '../theme'

declare module 'styled-components' {
  export interface DefaultTheme extends Styles {}
}

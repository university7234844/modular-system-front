const buildEnv = import.meta.env

const envs = {
  PHONE_NUMBER: buildEnv['VITE_PHONE_NUMBER'] ?? '',
}

export const getBuildEnv = (key: keyof typeof envs) => envs[key]

import { QueryClient, QueryClientProvider } from 'react-query'
import { BrowserRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'

import { ApiProvider } from './api/api-client/base/api-client.provider'
import { HttpClient, HttpClientProvider } from './api/http-client'
import AppRoutes from './routes'
import { ModalContextProvider } from './shared/hooks/use-modal/modal-context.provider'
import { UserContextProvider } from './state/user/context/user-context-provider'
import { theme } from './theme'

const queryClient = new QueryClient()

const httpClient = new HttpClient({
  basePath: 'https://localhost:3000/',
})

export const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <QueryClientProvider client={queryClient}>
        <HttpClientProvider httpClient={httpClient}>
          <ApiProvider>
            <UserContextProvider>
              <ModalContextProvider>
                <BrowserRouter>
                  <AppRoutes />
                </BrowserRouter>
              </ModalContextProvider>
            </UserContextProvider>
          </ApiProvider>
        </HttpClientProvider>
      </QueryClientProvider>
    </ThemeProvider>
  )
}

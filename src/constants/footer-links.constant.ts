import { Link } from '@/types'

export const footerLinks: Link[] = [
  { url: 'https://vk.com/mf_vsu', title: 'VK' },
  { url: 'https://www.instagram.com/fmiit_vsu/', title: 'Instagram' },
  { url: 'https://vsu.by/', title: 'Website' },
]

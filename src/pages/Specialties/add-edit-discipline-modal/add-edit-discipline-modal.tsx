import { FC } from 'react'
import { useForm } from 'react-hook-form'

import { Discipline } from '@/api/api-client/discipline/types'
import { BaseModalProps, Button, Input, Modal } from '@/shared/components'
import {
  useAddDisciplineMutation,
  useDeleteDisciplineMutation,
  useUpdateDisciplineMutation,
} from '@/state'

import { LeftButtons, ModalButtons, ModalContent, ModalHeader } from './styles'

export interface ModalProps extends BaseModalProps, Partial<Discipline> {
  isEdit: boolean
}

export const AddEditDisciplineModal: FC<ModalProps> = ({
  onClose,
  id,
  shortTitle,
  title,
  isEdit,
}) => {
  const { register, getValues, handleSubmit } = useForm({
    defaultValues: {
      id,
      shortTitle: shortTitle ?? '',
      title: title ?? '',
    },
    mode: 'onSubmit',
  })

  const { mutate: deleteDisciplineMutation } = useDeleteDisciplineMutation()

  const onSuccess = () => {
    onClose()
  }

  const { mutate: addDisciplineMutation } = useAddDisciplineMutation({
    onSuccess,
  })
  const { mutate: updateDisciplineMutation } = useUpdateDisciplineMutation({
    onSuccess,
  })
  const submitHandler = () => {
    const { id, ...restDiscipline } = getValues()

    if (isEdit && id) {
      updateDisciplineMutation({ id, discipline: restDiscipline })
    } else {
      addDisciplineMutation({ ...restDiscipline })
    }
  }

  const handleDelete = () => {
    const id = getValues('id')
    if (id) deleteDisciplineMutation(id)
    onClose()
  }

  return (
    <Modal>
      <ModalContent className="modal-content">
        <ModalHeader>{isEdit ? 'Редактирование' : 'Добавление'}</ModalHeader>
        <Input {...register('title')} label="Название:" />
        <Input {...register('shortTitle')} label="Аббревиатура:" />
        <ModalButtons className="modal-buttons">
          <LeftButtons className="right-buttons">
            <Button type="submit" onClick={handleSubmit(submitHandler)}>
              {isEdit ? 'Сохранить' : 'Добавить'}
            </Button>
            {isEdit && (
              <Button className="delete-button" onClick={handleDelete}>
                Удалить
              </Button>
            )}
          </LeftButtons>
          <Button onClick={onClose}>Отмена</Button>
        </ModalButtons>
      </ModalContent>
    </Modal>
  )
}

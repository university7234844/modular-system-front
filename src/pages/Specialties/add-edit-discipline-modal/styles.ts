import styled, { css } from 'styled-components'

export const StyledModal = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5); /* Semi-transparent gray background */
  display: flex;
  justify-content: center;
  align-items: center;
`

export const ModalContent = styled.div`
  background-color: #333;
  padding: 20px;
  border-radius: 5px;
  display: flex;
  flex-direction: column;
  ${({ theme }) => css`
    gap: ${theme.spaces.xs2}px;
    min-width: ${theme.modalWidths.s};
  `}
  width: fit-content;
  max-width: 100%;
`

export const ModalHeader = styled.h2`
  margin: 0;
`
export const ModalButtons = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`
export const LeftButtons = styled.div`
  display: flex;
  gap: 10px;
`

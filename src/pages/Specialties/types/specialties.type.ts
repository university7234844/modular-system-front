export type Specialty = {
  id: string
  shortTitle: string
  title: string
}

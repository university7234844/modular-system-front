import { Discipline } from '@/api/api-client/discipline/types'

export type SpecialtiesDataProps = {
  disciplines: Discipline[]
}

import styled, { css } from 'styled-components'

export const ListContainer = styled.div`
  ${({ theme }) => css`
    ${theme.getTypography('desktop', 'body', 'body2')}

    background-color: ${theme.colors.darkGray};
    padding: ${theme.spaces.sm}px;
  `}
`

export const List = styled.div`
  display: flex;
  flex-direction: column;
  align-items: normal;
`

export const Search = styled.input`
  ${({ theme }) => css`
    ${theme.getTypography('desktop', 'body', 'body2')}

    padding: ${theme.spaces.xs2}px;
    border: 1px solid ${theme.colors.lightSkyBlue};

    &:focus {
      border: 1px solid ${theme.colors.brightSkyBlue};
      outline: none;
    }
  `}
`

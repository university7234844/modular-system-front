import { Discipline } from '@/api'
import { useModal } from '@/shared/hooks'
import { useSingedUser } from '@/state'

import { AddEditDisciplineModal } from '../../add-edit-discipline-modal'
import { ListItem } from './styles'

export const DisciplineItem = ({ id, shortTitle, title }: Discipline) => {
  const { showModal } = useModal(AddEditDisciplineModal, {
    id,
    shortTitle,
    title,
    isEdit: true,
  })

  const { user } = useSingedUser()

  const handleShowEdit = () => {
    if (user) {
      showModal()
    }
  }

  return (
    <ListItem onClick={handleShowEdit}>
      <div>{shortTitle}</div>
      <div>{title}</div>
    </ListItem>
  )
}

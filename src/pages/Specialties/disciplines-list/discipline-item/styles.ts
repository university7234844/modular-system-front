import styled, { css } from 'styled-components'

export const ListItem = styled.div`
  ${({ theme }) => css`
    padding: ${theme.spaces.xs}px;
    margin-bottom: ${theme.spaces.xs2}px;
    background: ${theme.colors.lightGray};
    border-left: ${theme.spaces.xs2}px solid ${theme.colors.brightSkyBlue};

    &:hover {
      background-color: ${theme.colors.hoverLightGray};
      border-left: ${theme.spaces.xs3}px solid ${theme.colors.brightOrange};
    }
  `}

  width: 100%;
  text-align: left;
  text-decoration: none;
  transition: all 0.3s ease-out;
  box-sizing: border-box;
  word-wrap: break-word;
`

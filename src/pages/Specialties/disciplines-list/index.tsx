import { FC, useState } from 'react'

import { Discipline } from '@/api/api-client/discipline/types'
import { Button } from '@/shared/components'
import { useModal } from '@/shared/hooks'
import { useSingedUser } from '@/state'

import { AddEditDisciplineModal } from '../add-edit-discipline-modal'
import { SpecialtiesDataProps } from '../types'
import { DisciplineItem } from './discipline-item/disciplines-list'
import { List, ListContainer, Search } from './styles'

interface Props extends SpecialtiesDataProps {
  onSearch: (value: Discipline[]) => void
}

export const DisciplinesList: FC<Props> = ({ disciplines, onSearch }) => {
  const [searchTerm, setSearchTerm] = useState('')
  const { showModal } = useModal(AddEditDisciplineModal, { isEdit: false })
  const { user } = useSingedUser()
  const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    const letter = event.target.value

    if (letter) {
      setSearchTerm(letter)
      const filtered = disciplines.filter(s =>
        s.shortTitle.toLowerCase().startsWith(letter.toLowerCase())
      )

      onSearch(filtered)
    } else {
      onSearch([])
      setSearchTerm('')
    }
  }

  return (
    <List className="list">
      <Search
        placeholder="Поиск по аббревиатуре"
        value={searchTerm}
        onChange={handleSearch}
      />
      <ListContainer className="list-container">
        {disciplines.map(({ id, ...props }) => (
          <DisciplineItem {...props} id={id} key={id} />
        ))}
      </ListContainer>
      {user && <Button onClick={showModal}>Добавить</Button>}
    </List>
  )
}

import React, { useState } from 'react'

import { Discipline } from '@/api/api-client/discipline/types'
import { useDisciplineQuery } from '@/state/discipline/queries/use-discipline-query'

import { DisciplinesList } from './disciplines-list'

const SpecialtiesPage: React.FC = () => {
  const [filtered, setFiltered] = useState<Discipline[]>([])

  const { data: specialties } = useDisciplineQuery()

  const handleSearch = (filtered: Discipline[]) => {
    setFiltered(filtered)
  }

  return (
    <>
      {specialties ? (
        <DisciplinesList
          disciplines={filtered.length ? filtered : specialties.records}
          onSearch={handleSearch}
        />
      ) : (
        <p>Loading...</p>
      )}
    </>
  )
}

export default SpecialtiesPage

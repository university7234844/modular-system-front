import { borderRadiuses } from './border-radiuses'
import { colors } from './colors'
import { modalWidths } from './modal-widths'
import { above, below } from './screen-sizes'
import { spaces } from './spaces'
import { getTypography, typography } from './typography'
import { zIndexes } from './z-indexes'

export {
  Container,
  ImageContainer,
  NormalizedContainer,
  PageContent,
} from './common-components'
export { SCREEN_SIZES } from './screen-sizes'

export const theme = {
  name: 'Light',
  above,
  below,
  colors,
  typography,
  getTypography,
  zIndexes,
  spaces,
  modalWidths,
  borderRadiuses,
} as const

export type Styles = typeof theme

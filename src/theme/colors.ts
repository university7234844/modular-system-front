export const colors = {
  brightSkyBlue: '#12c2f9',
  lightSkyBlue: '#ade0ff',
  brightOrange: '#f05d23',
  dark: '#393939',
  lightGray: '#494847',
  hoverLightGray: '#61605e',
  darkGray: '#333333',
  darkTransparent: 'rgba(0, 0, 0, 0.7)',
  while: '#ffffff',
} as const

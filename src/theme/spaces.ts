export const spaces = {
  xs3: 4,
  xs2: 8,
  xs: 16,
  sm: 24,
  md: 32,
  lg: 40,
  xl: 48,
  xl2: 56,
  xl3: 64,
  xl4: 72,
  xl5: 80,
  xl6: 88,
  xl7: 96,
  xl8: 104,
  xl9: 112,
  xl10: 120,
} as const

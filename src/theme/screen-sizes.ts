import { css } from 'styled-components'

export const SCREEN_SIZES = {
  mobile: 320,
  tablet: 768,
  laptop: 992,
  desktopXS: 1200,
} as const

type ScreenSizes = keyof typeof SCREEN_SIZES
type BreakPoints = {
  [key in ScreenSizes]: (
    ...args: Parameters<typeof css>
  ) => ReturnType<typeof css>
}

const devices = Object.keys(SCREEN_SIZES) as Array<ScreenSizes>

export const above = devices.reduce((acc, key) => {
  acc[key] = (...args) => css`
    @media (min-width: ${SCREEN_SIZES[key]}px) {
      ${css(...args)}
    }
  `
  return acc
}, {} as BreakPoints)

export const below = devices.reduce((acc, key) => {
  acc[key] = (...args) => css`
    @media (max-width: ${SCREEN_SIZES[key]}px) {
      ${css(...args)}
    }
  `
  return acc
}, {} as BreakPoints)

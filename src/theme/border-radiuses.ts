export const borderRadiuses = {
  s: '2px',
  m: '6px',
  l: '8px',
}

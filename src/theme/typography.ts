import { css } from 'styled-components'

import type { ComponentVariant, Device, Typography } from '../types'

export const typography: Typography = {
  desktop: {
    title: {
      large: {
        fontSize: 24,
        fontWeight: 600,
        lineHeight: 28,
      },
      blog: {
        fontSize: 22,
        fontWeight: 600,
        lineHeight: 28,
      },
      title1: {
        fontSize: 20,
        fontWeight: 600,
        lineHeight: 24,
      },
      title2: {
        fontSize: 24,
        fontWeight: 600,
        lineHeight: 28,
      },
      title3: {
        fontSize: 22,
        fontWeight: 100,
        lineHeight: 28,
      },
      title4: {
        fontSize: 20,
        fontWeight: 100,
        lineHeight: 24,
      },
      title5: {
        fontSize: 18,
        fontWeight: 100,
        lineHeight: 24,
      },
    },
    body: {
      body1: {
        fontSize: 24,
        fontWeight: 100,
        lineHeight: 36,
      },
      body2: {
        fontSize: 18,
        fontWeight: 100,
        lineHeight: 24,
      },
      body3: {
        fontSize: 16,
        fontWeight: 100,
        lineHeight: 24,
      },
    },
    button: {
      normal: {
        fontSize: 18,
        fontWeight: 600,
        lineHeight: 24,
      },
      medium: {
        fontSize: 16,
        fontWeight: 500,
        lineHeight: 20,
      },
      small: {
        fontSize: 18,
        fontWeight: 300,
        lineHeight: 23,
      },
    },
    preText: {
      text1: {
        fontSize: 12,
        fontWeight: 500,
        lineHeight: 25,
      },
    },
  },
  mobile: {
    title: {
      large: {
        fontSize: 36,
        fontWeight: 800,
        lineHeight: 45,
      },
      blog: {
        fontSize: 30,
        fontWeight: 800,
        lineHeight: 40,
      },
      title1: {
        fontSize: 26,
        fontWeight: 800,
        lineHeight: 38,
      },
      title2: {
        fontSize: 24,
        fontWeight: 800,
        lineHeight: 28,
      },
      title3: {
        fontSize: 18,
        fontWeight: 700,
        lineHeight: 23,
      },
      title4: {
        fontSize: 16,
        fontWeight: 600,
        lineHeight: 20,
      },
      title5: {
        fontSize: 16,
        fontWeight: 100,
        lineHeight: 20,
      },
    },
    body: {
      body1: {
        fontSize: 18,
        fontWeight: 300,
        lineHeight: 23,
      },
      body2: {
        fontSize: 16,
        fontWeight: 300,
        lineHeight: 21,
      },
      body3: {
        fontSize: 14,
        fontWeight: 400,
        lineHeight: 18,
      },
      body4: {
        fontSize: 14,
        fontWeight: 300,
        lineHeight: 18,
      },
    },
    button: {
      normal: {
        fontSize: 16,
        fontWeight: 600,
        lineHeight: 20,
      },
      medium: {
        fontSize: 12,
        fontWeight: 500,
        lineHeight: 15,
      },
    },
    preText: {
      text1: {
        fontSize: 12,
        fontWeight: 500,
        lineHeight: 15,
      },
    },
  },
}

export function getTypography<D extends Device, V extends ComponentVariant>(
  device: D,
  variant: V,
  size: keyof Typography[D][V]
) {
  const { fontSize, fontWeight, lineHeight } = typography[device][variant][size]

  return css`
    font-size: ${fontSize}px;
    font-weight: ${fontWeight};
    line-height: ${lineHeight}px;
  `
}

import styled, { css } from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  margin: 0 auto;
  padding-top: 82px;
  position: relative;
  width: 100%;
`

export const NormalizedContainer = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  margin: 0 auto;
  max-width: 1440px;
  width: 100%;
`

export const PageContent = styled.div`
  overflow: hidden;

  ${({ theme }) => css`
    color: ${theme.colors.secondary};
    width: 100%;

    & > div:not(:first-child) {
      margin-top: ${theme.spaces.xl10}px;
    }

    ${theme.below.laptop`
      & > div:not(:first-child) {
        margin-top: ${theme.spaces.xl3}px;
      }
    `}

    .decorative-item {
      ${theme.below.laptop`
        display: none;
      `}
    }
  `}
`

export const ImageContainer = styled.div`
  height: 100%;
  position: relative;
  width: 100%;
`

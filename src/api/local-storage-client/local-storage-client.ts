import { User } from "@/state/types";

import { LocalStorageKeys } from "./keys";

export class LocalStorageClient {
    static getAccessToken() {
        return localStorage.getItem(LocalStorageKeys.accessToken)
    }

    static setAccessToken(token: string) {
        localStorage.setItem(LocalStorageKeys.accessToken, token)
    }

    static deleteAccessToken() {
        localStorage.removeItem(LocalStorageKeys.accessToken)
    }

    static setUserData(user: User) {
        localStorage.setItem(LocalStorageKeys.userData, JSON.stringify(user))
    }

    static getUserData() {
        return JSON.stringify(localStorage.getItem(LocalStorageKeys.userData)) as unknown as User
    }

    static deleteUserData() {
        localStorage.removeItem(LocalStorageKeys.userData)
    }
}
export enum LocalStorageKeys {
    accessToken = 'accessToken',
    userData = 'userData'
}
import { useContext } from 'react'

import { HttpClientContext } from '.'

export const useHttpClient = () => useContext(HttpClientContext)

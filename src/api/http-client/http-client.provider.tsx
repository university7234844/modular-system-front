import { createContext, PropsWithChildren } from 'react'

import { HttpClient } from './http-clients'

export interface HttpClientContextType {
  httpClient: HttpClient
}

export const HttpClientContext = createContext({} as HttpClientContextType)

interface HttpClientProviderProps
  extends PropsWithChildren,
    HttpClientContextType {}

export const HttpClientProvider = ({
  httpClient,
  children,
}: HttpClientProviderProps) => {
  return (
    <HttpClientContext.Provider value={{ httpClient }}>
      {children}
    </HttpClientContext.Provider>
  )
}

import axios, { AxiosInstance } from 'axios'

import { createAxiosResponseInterceptor } from '../axios'

interface HttpClientConstructor {
  basePath: string
}

export class HttpClient {
  private axiosInstance: AxiosInstance

  constructor({ basePath }: HttpClientConstructor) {
    this.axiosInstance = axios.create({
      baseURL: basePath,
      withCredentials: true,
    })
    createAxiosResponseInterceptor(this.axiosInstance)
  }

  public get<R>(url: string) {
    return this.axiosInstance.get<R>(url)
  }

  public post<R>(url: string, data?: unknown) {
    return this.axiosInstance.post<R>(url, data)
  }

  public put<R>(url: string, data: unknown) {
    return this.axiosInstance.put<R>(url, data)
  }
  public delete<R>(url: string) {
    return this.axiosInstance.delete<R>(url)
  }
}

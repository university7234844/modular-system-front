
import { AxiosInstance } from "axios";

import { LocalStorageClient } from "../local-storage-client";

export function createAxiosResponseInterceptor(axios: AxiosInstance) {
    const interceptor = axios.interceptors.response.use(
        (response) => response,
        (error) => {
            if (error.response.status !== 401) {
                return Promise.reject(error);
            }

            axios.interceptors.response.eject(interceptor);

            
            return axios
                .post("/auth/refresh")
                .then(({data: {accessToken}}) => {
                    error.response.config.headers["Authorization"] =
                        "Bearer " + accessToken;
                    
                    LocalStorageClient.setAccessToken(accessToken)

                    error.response.config._retry = true;

                    return axios(error.response.config);
                })
                .catch((error2) => {
                    return Promise.reject(error2);
                })
                .finally(() => createAxiosResponseInterceptor(axios));
        }
    );

    axios.interceptors.request.use( config => {
        const token = LocalStorageClient.getAccessToken();
        if (token) {
            config.headers['Authorization'] = 'Bearer ' + token;
        }
        config.headers['Content-Type'] = 'application/json';
        return config;
    },
        error => {
            Promise.reject(error)
        })
}
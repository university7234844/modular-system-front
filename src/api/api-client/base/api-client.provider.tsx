import { createContext, PropsWithChildren } from 'react'

import { useHttpClient } from '@/api/http-client/use-http-client'

import { AuthClient, DisciplineClient, UserClient } from '..'

type Clients = {
  authClient: AuthClient
  disciplineClient: DisciplineClient
  userClient: UserClient
}

export const ApiClientContext = createContext({} as Clients)

export const ApiProvider = ({ children }: PropsWithChildren) => {
  const { httpClient } = useHttpClient()

  const value = {
    authClient: new AuthClient(httpClient),
    disciplineClient: new DisciplineClient(httpClient),
    userClient: new UserClient(httpClient),
  }

  return (
    <ApiClientContext.Provider value={value}>
      {children}
    </ApiClientContext.Provider>
  )
}

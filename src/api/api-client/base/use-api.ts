import { useContext } from 'react'

import { ApiClientContext } from './api-client.provider'

export const useApi = () => useContext(ApiClientContext)

import { HttpClient } from '@/api/http-client'

import { LoginUserRequest } from './types/login-user.request'
import { LoginUserResponse } from './types/login-user.response'

export class AuthClient {
  private httpClient: HttpClient

  constructor(httpClient: HttpClient) {
    this.httpClient = httpClient
  }

  public loginUser(data: LoginUserRequest) {
    return this.httpClient.post<LoginUserResponse>('/auth/login', data)
  }

  public logoutUser() {
    return this.httpClient.post('/auth/logout')
  }

  public refreshLogin() {
    return this.httpClient.post<LoginUserResponse>('/auth/refresh')
  }
}

export interface Role {
  id: number
  name: string
}

export interface LoginUserResponse {
  id: number
  login: string
  roles: Role[]
}

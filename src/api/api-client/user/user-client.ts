import { HttpClient } from '@/api/http-client'
import { User } from '@/state/types'



export class UserClient {
  private httpClient: HttpClient

  constructor(httpClient: HttpClient) {
    this.httpClient = httpClient
  }

  public getCurrentUser() {
    return this.httpClient.get<User>('/user/current')
  }
    
    
}

import { HttpClient } from '@/api/http-client'

import { Discipline, getDisciplineResponse } from './types'

export class DisciplineClient {
  private httpClient: HttpClient

  constructor(httpClient: HttpClient) {
    this.httpClient = httpClient
  }

  public getDiscipline() {
    return this.httpClient.get<getDisciplineResponse>('/discipline')
  }
  public deleteDiscipline(id: number) {
    return this.httpClient.delete(`/discipline/${id}`)
  }
  public addDiscipline(discipline: Omit<Discipline, 'id'>) {
    return this.httpClient.post('/discipline', discipline)
  }
  public updateDiscipline(
    id: number,
    discipline: Partial<Omit<Discipline, 'id'>>
  ) {
    return this.httpClient.put(`/discipline/${id}`, discipline)
  }
}

export interface Discipline {
  id: number
  title: string
  shortTitle: string
}

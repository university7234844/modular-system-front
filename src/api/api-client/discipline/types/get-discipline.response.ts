import { Discipline } from './discipline'

export interface getDisciplineResponse {
  totalRecordsNumber: number
  records: Discipline[]
}

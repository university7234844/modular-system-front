import { Route, Routes } from 'react-router-dom'

import { HomePage } from '@/pages/main'
import { NotFound } from '@/pages/not-found'
import SpecialtiesPage from '@/pages/specialties'
import { PageTemplate } from '@/shared/components'

export default function AppRoutes() {
  return (
    <Routes>
      <Route element={<PageTemplate />}>
        <Route path="/specialties" element={<SpecialtiesPage />} />
        <Route path="/" element={<HomePage />} />
        <Route path="*" element={<NotFound />} />
      </Route>
    </Routes>
  )
}

import react from '@vitejs/plugin-react'
import * as fs from 'fs/promises'
import { fileURLToPath, URL } from 'url'
import { defineConfig } from 'vite'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: [
      {
        find: '@',
        replacement: fileURLToPath(new URL('./src', import.meta.url)),
      },
    ],
  },
  envDir: './',
  server: {
    https: {
      key: await fs.readFile('./https/cert.key'),
      cert: await fs.readFile('./https/cert.crt'),
    },
  },
  build: {
    minify: false,
    rollupOptions: {
      output: {
        manualChunks: {},
      },
    },
  },
})
